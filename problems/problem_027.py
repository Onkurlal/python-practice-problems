# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    return max(values) if values else None

print(max_in_list([4,5,67,8]))
