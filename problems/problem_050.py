# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]
def halve_the_list(lis):
    if len(lis) % 2 == 0:
        median = len(lis)//2
        return lis[:median], lis[median:]
    else:
        median = len(lis)//2
        return lis[:median+1], lis[median+1:]

print(halve_the_list([1, 2, 3, 4 ,6,5,7,5]))
