# Write a function that meets these requirements.
#
# Name:       shift_letters
# Parameters: a string containing a single word
# Returns:    a new string with all letters replaced
#             by the next letter in the alphabet
#
# If the letter "Z" or "z" appear in the string, then
# they would get replaced by "A" or "a", respectively.
#
# Examples:
#     * inputs:  "import"
#       result:  "jnqpsu"
#     * inputs:  "ABBA"
#       result:  "BCCB"
#     * inputs:  "Kala"
#       result:  "Lbmb"
#     * inputs:  "zap"
#       result:  "abq"
#
# You may want to look at the built-in Python functions
# "ord" and "chr" for this problem
def shift_letters(str):
    shifted_str = ''
    for char in str:
        shifted_letter = ''
        if char == 'z':
            shifted_letter = 'a'
        elif char == 'Z':
            shifted_letter = 'A'
        else:
            shifted_letter = chr(ord(char)+1)
        shifted_str += shifted_letter
    return shifted_str

print(shift_letters("Zap"))
