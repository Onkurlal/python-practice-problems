# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    checks_dict ={'upper':0, 'lower':0, 'digit':0,'special':0,'length':0}
    for char in password:
        if char.isupper():
            checks_dict['upper'] += 1
        elif char.islower():
            checks_dict['lower'] += 1
        elif char.isdigit():
            checks_dict['digit'] += 1
        elif char == '$' or char == '!' or char == '@':
            checks_dict['special'] += 1
    checks_dict['length'] = len(password)
    if checks_dict['upper'] >= 1 and checks_dict['lower'] >= 1 and checks_dict['digit'] >= 1 and checks_dict['special'] >= 1 and checks_dict['length'] >= 6 and checks_dict['length'] <= 12:
        return 'Valid password!'
    else:
        return "Try Again!"

print(check_password('P!6assword'))
